const axios = require("axios").default;

const { Client, Intents } = require("discord.js");
const { config } = require("./config");

const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
const tokenChainKey = "arbitrum:0x1622bF67e6e5747b81866fE0b85178a93C7F86e3";
// if you don't want to update a server ran by a bozo
const kekList = ["885769815552323626"];

const updateValues = async () => {
  try {
    const [responseSupply, responseMetrics, responsePrice] = await Promise.all([
      axios.get(`${config.UMAMI_API_URL}/token/metrics`),
      axios.get(`${config.UMAMI_API_URL}/staking/metrics/current?keys=apr`),
      axios.get(`${config.LLAMA_API_URL}/prices/current/${tokenChainKey}`),
    ]);

    const supply = responseSupply.data.marinating;
    const apr = parseFloat(responseMetrics.data.metrics[0].value).toFixed(2);
    const umamiPrice = responsePrice.data.coins[tokenChainKey].price;

    const tvl = parseFloat(supply) * parseFloat(umamiPrice);

    if (!isNaN(tvl) && !isNaN(apr)) {
      client.guilds.cache.map((guild) => {
        // update every server the bot is connected to
        if (guild.me) {
          const nickname = kekList.includes(guild.id.toString())
            ? "ON STRIKE"
            : `$${parseInt(tvl).toLocaleString()}`;
          guild.me.setNickname(nickname);
          const activity = kekList.includes(guild.id.toString())
            ? "ON STRIKE"
            : `Marinating - ${apr}% apr`;
          client.user.setActivity(activity, {
            type: "WATCHING",
          });
        }
      });
    }
  } catch (error) {
    console.log("error : ", error);
  }
};

const run = async () => {
  // update once to init then run every 10 minutes
  await updateValues();
  setInterval(updateValues, 600000);
};

client.once("ready", () => {
  console.log("Ready!");

  run();
});

client.login(config.DISCORD_CLIENT_TOKEN);
